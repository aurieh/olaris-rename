module gitlab.com/olaris/olaris-rename

go 1.13

require (
	github.com/mholt/archiver/v3 v3.3.0
	github.com/ryanbradynd05/go-tmdb v0.0.0-20190901200645-e8dd22863620
	github.com/sirupsen/logrus v1.4.2
)
